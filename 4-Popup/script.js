popup = null;


function exitPopup(){
    try{
        document.body.removeChild(popup);
    }catch(e){}

    popup = null;
}
function createPopup(){
    if(popup != null){return}
    popup = document.createElement('div');
    popup.className = 'popup';

    var exitButton = document.createElement('button');
    exitButton.innerHTML = "X"
    exitButton.addEventListener("click",()=>{exitPopup()})
    exitButton.className = 'exitButton';
    popup.appendChild(exitButton);

    var header = document.createElement('h1');
    header.innerHTML = "This is a popup!"
    popup.appendChild(header);

    var p = document.createElement('p');
    p.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum felis ac quam facilisis malesuada. Integer sed lobortis odio. Suspendisse eleifend quam id felis vestibulum, eget tincidunt ex eleifend. Mauris finibus neque non nunc interdum pellentesque. Aenean non ex vitae ipsum pretium efficitur in id orci. Sed ornare, lacus eu imperdiet interdum, felis metus interdum tortor, non convallis mauris, felis metus interdum tortor, Donec bibendum felis ac quam facilisis malesuada."
    popup.appendChild(p);

    document.body.appendChild(popup);
    fadePopup(popup);
}
function fadePopup(p){
    var op = 0;
    var opInt = setInterval(()=>{
        if(op >= 1){
            clearInterval(opInt);
        };
        p.style.opacity = op;
        op+=0.01;
    },10)
}
