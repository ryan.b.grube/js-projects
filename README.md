## Beginners
- Background color changer: [Color changing app](https://github.com/JS-Beginners/color-changing-app) ✓ (didnt use guide)
- Random quotes generator: [Random quote generator](https://dev.to/rutikwankhade/learn-javascript-concepts-by-building-a-random-quote-generator-3jan) ✓ (didnt use guide)
- Counter project: [Counter JavaScript project](https://jsbeginners.com/counter-project/) ✓ (didnt use guide)
- Create a modal: [Create a Modal (Popup) with HTML/CSS and JavaScript](https://www.youtube.com/watch?v=XH5OW46yO8I) ✓ (didnt use guide)
- Calculator : [JS Calculator project](https://github.com/JS-Beginners/Calculator-JavaScript-Project) ✓ (didnt use guide) 
- BMI Calculator: [BMI Calculator JavaScript project](https://projectnotes.org/it-projects/bmi-calculator-in-javascript-with-source-code/) ✓ (didnt use guide)
- Event countdown timer: [Countdown Timer javascript project](https://www.youtube.com/watch?v=4_o3wO6aawg)
- Tip Calculator: [Build a tip calculator JavaScript](https://www.freecodecamp.org/news/how-to-build-a-tip-calculator-with-html-css-and-javascript/)
- Word counter: [Word counter project](https://www.javascripttutorial.net/javascript-dom/javascript-word-counter/)
- Day of Week: [Day of week javascript project](https://jsbeginners.com/javascript-day-of-the-week-project/)
- Number guessing game: [Number guessing game project](https://jsbeginners.com/javascript-number-guessing-game/)
- Height conversion: [Height conversion project](https://jsbeginners.com/javascript-height-converter-project/)
- Coin toss: [Coin toss project](https://jsbeginners.com/coin-toss-game/)
- Rock paper scissor game: [Rock paper scissor game project](https://jsbeginners.com/rock-paper-scissors-javascript-game/)

## Intermediate
- Image slider: [Image slider project](https://www.c-sharpcorner.com/article/creating-an-image-slider-using-javascript-html-and-css-only/)
- Product filter project : [Product filter project](https://www.youtube.com/watch?v=kkih4C80QTk)
- To-do List : [To do list project](https://www.youtube.com/watch?v=bivmcufOJ34)
- Grocery list: [Grocery list project](https://jsbeginners.com/javascript-grocery-list-project/)
- Budget tracker : [Budget tracker project](https://www.youtube.com/watch?v=nEPJQB0RL80)
- Weather project: [Weather project project](https://www.youtube.com/watch?v=fffX2ge3wHs)
- Analogue clock: [Clock project](https://dev.to/nepalilab/create-an-analog-clock-with-vanilla-javascript-for-beginners-3ibb)
- Contact form : [Contact form project](Contact%20form%20project)
- Quiz : [Quiz project](https://dev.tolink/)
- Typing test: [Typing test project](https://www.geeksforgeeks.org/design-a-typing-speed-test-game-using-javascript/)
- World Scrabble game: [World Scrabble game project](https://www.youtube.com/watch?v=f_Tq7GPRJkQ)
- Drum kit game: [Drum kit game project](https://code-projects.org/javascript-drum-kit-with-source-code/)

## Pros
- Real time chat app: [Real time chat app project](https://www.youtube.com/watch?v=rxzOqP9YwmM)
- Text-to-Speech: [Text-to-Speech project](https://www.section.io/engineering-education/text-to-speech-in-javascript/)
- Search engine: [Search engine project](https://blog.logrocket.com/write-your-own-search-engine-using-node-js-and-elastic/)
- Distance calculator with Google API
- Shopping platform

The original article can be found [here.](https://dev.to/devwriteups/30-javascript-project-ideas-for-beginners-to-pro-developer-with-resources-175k)